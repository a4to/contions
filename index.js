
const fs = require('fs');
const path = require('path');

const libdir = path.join(__dirname, 'lib');
const files = fs.readdirSync(libdir);
const functions = {};

files.forEach((file) => {
  const filePath = path.join(libdir, file);
  if (fs.statSync(filePath).isFile()) {
    const module = require(filePath);
    Object.keys(module).forEach((key) => {
      functions[key] = module[key];
    });
  }
});

module.exports = functions;


