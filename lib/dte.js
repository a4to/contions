
module.exports = {
  utcdte: new Date().toUTCString(),
  isodte: new Date().toISOString(),
  shortdte: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'short', year: 'numeric' }),
  longdte: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'long', year: 'numeric' }),
  fulldte: new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'long', year: 'numeric', weekday: 'long' }),
  dte: shortdte
}
