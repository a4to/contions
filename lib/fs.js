
const fs = require('fs');

const writeFile = (filepath, content) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(filepath, content, (err) => {
      if (err) { reject(err); } else { resolve(); }
    });
  })
};

const readFile = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, (err, data) => {
      if (err) { reject(err); } else { resolve(data); }
    });
  });
};

const appendFile = (filepath, content) => {
  return new Promise((resolve, reject) => {
    fs.append(filepath, content, (err) => {
      if (err) { reject(err); } else { resolve(); }
    });
  });
};

const deleteFile = (filepath) => {
  return new Promise((resolve, reject) => {
    fs.unlink(filepath, (err) => {
      if (err) { reject(err); } else { resolve(); }
    });
  });
};


module.exports = { wf: writeFile, rf: readFile, af: appendFile, df: deleteFile };
